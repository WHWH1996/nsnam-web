---
layout: page
title: Call for Posters, Demos, and Works-In-Progress
permalink: /research/wns3/wns3-2017/call-for-posters/
---
WNS3 invites your participation in the workshop with a poster presentation, a demo presentation, or a short talk about work-in-progress.

Our goal is to create more opportunities for discussion in the workshop. The scope for this call is similar to that of the CFP for the papers track &ndash; topics of interest include, but are not limited to:

  * new models, devices, protocols and applications for ns-3
  * using ns-3 in modern networking research
  * comparison with other network simulators and emulators
  * speed and scalability issues for ns-3
  * multiprocessor and distributed simulation with ns-3, including the use of GPUs
  * validation of ns-3 models
  * credibility and reproducibility issues for ns-3 simulations
  * user experience issues of ns-3
  * frameworks for the definition and automation of ns-3 simulations
  * post-processing, visualisation and statistical analysis tools for ns-3
  * models ported from other simulators to ns-3 and models ported from ns-3 to other simulation environments
  * using real code for simulation with ns-3 and using ns-3 code in network applications
  * integration of ns-3 with testbeds, emulators, and other simulators or tools
  * using ns-3 API from programming languages other than C++ or Python
  * porting ns-3 to unsupported platforms
  * network emulation with ns-3
  * using ns-3 in education and teaching

### Submission Instructions

To propose a demonstration, poster, or work-in-progress presentation, authors should submit a one or two-page extended abstract in PDF format via **EMAIL to the Technical Program Co-Chairs listed below**. Use the ACM "sigconf" proceedings format&nbsp;(<a href="http://www.acm.org/publications/proceedings-template" target="_blank">http://www.acm.org/publications/proceedings-templates</a>).&nbsp; The extended abstract should include the basic idea, the scope, and significance of your proposal.

For demonstrations, be as specific as possible in describing what you will demonstrate. Include an estimate of the space, and setup time needed for your demonstration. Also, provide information about any equipment that you might need for the demonstration.

For WIP, specify an estimation of time needed for your short talk.

Accepted Posters/Demos/WIPs abstracts will be posted on the ns-3 web site but will not be published in ACM digital libraries. At least one author of each accepted poster/demo/WIP must register and present at the workshop.

### Technical Program Co-Chairs

Eric Gamess (egamess at&nbsp;gmail.com)

Brian Swenson (bpswenson at&nbsp;gmail.com)

### Important Dates

Demo, poster, work-in-progress deadline: <s>Sunday April 2, 2017</s> Extended to Sat. June 3

Notification of acceptance: <s>Sunday April 9, 2017</s> Rolling acceptance.

Workshop date : June 13-14, 2017 (Poster session on June 13)

&nbsp;
