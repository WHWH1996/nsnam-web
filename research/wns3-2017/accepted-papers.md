---
layout: page
title: Accepted Papers
permalink: /research/wns3/wns3-2017/accepted-papers/
---
  * **Natale Patriciello**. A SACK-based Conservative Loss Recovery Algorithm for ns-3 TCP: a Linux-inspired Proposal
  * **Hany Assasa and Joerg Widmer**. Extending the IEEE 802.11ad Model: Scheduled Access, Spatial Reuse, Clustering, and Relaying
  * **Richard Rouil, Fernando J. Cintr&oacute;n, Aziza Ben Mosbah and Samantha Gamboa**. Implementation and Validation of an LTE D2D Model for ns-3
  * **Biljana Bojovic, Melchiorre Danilo Abrignani, Marco Miozzo, Lorenza Giupponi and Nicola Baldo**. Towards LTE-Advanced and LTE-A Pro Network Simulations: Implementing Carrier Aggregation in LTE Module of ns-3
  * **Hemin Yang, Chuanji Zhang and George Riley**. Support Multiple Auxiliary TCP/UDP Connections in SDN Simulations Based on ns-3
  * **Rohan Patidar, Sumit Roy, Thomas Henderson and Amrutha Chandramohan**. Link-to-System Mapping for ns-3 Wi-Fi OFDM Error Models
  * **Ioannis Selinis, Konstantinos Katsaros, Seiamak Vahid and Rahim Tafazolli**. Exploiting the Capture Effect on DSC and BSS Color in Dense IEEE 802.11ax Deployments
  * **Matteo Franchi, Tommaso Pecorella, Alessandro Ridolfi, Romano Fantacci and Benedetto Allotta**. Kinematic Constraints and ns-3 Mobility Models: the AUV Issue
  * **Ankit Deepak, Shravya K.S. and Mohit Tahiliani**. Design and Implementation of AQM Evaluation Suite for ns-3
  * **Harald Ott, Konstantin Miller and Adam Wolisz**. Simulation Framework for HTTP-Based Adaptive Streaming Applications
  * **Kriti Nagori, Meenakshy Balachandran, Ankit Deepak, Mohit P. Tahiliani and B.R Chandavarkar**. Common TCP Evaluation Suite for ns-3: Design, Implementation and Open Issues
  * **Pasquale Imputato and Stefano Avallone**. Traffic Differentiation and Multiqueue Networking in ns-3
  * **Menglei Zhang, Michele Polese, Marco Mezzavilla, Sundeep Rangan and Michele Zorzi**. ns-3 Implementation of the 3GPP MIMO Channel Model for Frequency Spectrum above 6 GHz
  * **Truc Anh Nguyen and James Sterbenz**. An Implementation and Analysis of SCPS-TP in ns-3
  * **Jo&atilde;o Loureiro, Pedro Santos, Raghuraman Rangarajan and Eduardo Tovar**. Simulation Module and Tools for XDense Sensor Network
  * **Helder Fontes, Rui Campos and Manuel Ricardo**. A Trace-based ns-3 Simulation Approach for Perpetuating Real-World Experiments
