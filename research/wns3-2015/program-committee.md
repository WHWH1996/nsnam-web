---
layout: page
title: Program Committee
permalink: /research/wns3/wns3-2015/program-committee/
---
### **General Chair**

  * Nicola Baldo, Centre Tecnològic de Telecomunicacions de Catalunya (CTTC) <nbaldo@cttc.es>

### **Technical Program Committee Co-Chairs**

  * Peter D. Barnes, Lawrence Livermore National Laboratory <barnes26@llnl.gov>
  * Hajime Tazaki, University of Tokyo, <tazaki@wide.ad.jp>

### **Proceedings Chair**

  * Eric Gamess, Universidad Central de Venezuela <egamess@gmail.com>

### **Technical Program Committee Members**

  * Alexander Afanasyev, UCLA
  * Manuel Ricardo, INESC Porto
  * Thierry Turletti, INRIA
  * Marco Miozzo, CTTC
  * Mathieu Lacage, Alcmeon
  * Nicola Baldo, CTTC
  * Sam Jansen, StarLeaf
  * Eric Gamess, Universidad Central de Venezuela
  * Sebastien Deronne, Alcatel-Lucent Bell
  * Cristiano Tapparello, University of Rochester
  * Peter D. Barnes, Jr., Lawrence Livermore National Laboratory
  * Hajime Tazaki, University of Tokyo
  * Mohit Tahiliani, National Institute of Technology Karnataka
  * Giovanni Stea, University of Pisa
  * Lalith Suresh, Deutsche Telekom Labs
  * Carlos Moreno, Universidad Central de Venezuela
  * Tom Henderson, University of Washington
  * Ruben Merz, Swisscom
  * Ken Renard, Army Research Lab
