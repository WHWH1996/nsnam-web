---
layout: page
title: WNS3 2020 Training
permalink: /research/wns3/wns3-2020/training/
---

ns-3 training sessions will be held on Monday June 15 and Tuesday June 16, prior to the Workshop on ns-3. Several ns-3 maintainers will conduct the training.

Details on training sessions will be announced at a later date.
