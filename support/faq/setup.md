---
title: Setup
layout: page
permalink: /support/faq/setup/
---
<div id="toc_container" class="toc_wrap_right no_bullets">
  <ul class="toc_list">
    <li>
      <a href="#Downloading_stable_releases"><span class="toc_number toc_depth_1">1</span> Downloading stable releases</a>
    </li>
    <li>
      <a href="#Downloading_development_tree"><span class="toc_number toc_depth_1">2</span> Downloading development tree</a>
    </li>
    <li>
      <a href="#Building"><span class="toc_number toc_depth_1">3</span> Building</a>
    </li>
    <li>
      <a href="#Using_cygwin"><span class="toc_number toc_depth_1">4</span> Using cygwin</a>
    </li>
    <li>
      <a href="#Using_mingw"><span class="toc_number toc_depth_1">5</span> Using mingw</a>
    </li>
  </ul>
</div>

# <span id="Downloading_stable_releases">Downloading stable releases</span>

ns-3 makes unpackaged source releases only at this time. Downloading the latest stable release should be straightforward, from the main project page. Archived older releases are also linked there.

# <span id="Downloading_development_tree">Downloading development tree</span>

Some people may want to work with our development branch, which is ns-3-dev. We've introduced a framework to ease working with the development version. It is called ns-3-allinone. As of early January 2009, downloading of optional components to ns-3 has been delegated to a download script in this ns-3-allinone framework.

Using the development tree requires mercurial. Try the following steps:

<pre>hg clone http://code.nsnam.org/ns-3-allinone
 cd ns-3-allinone
 ./download.py
</pre>

You should then have the following directory structure in ns-3-allinone/

<pre>build.py*     dist.py*      ns-3-dev/   pybindgen/  util.py
 constants.py  download.py*  nsc/        README
</pre>

If you start using these development trees a lot, you should consider learning more about mercurial and looking at our [mercurial page](/developers/tools/mercurial).

# <span id="Building">Building</span>

Once users have downloaded ns-3 (a developement or stable version), they will want to then invoke build.py to start a coordinated build.

<pre>./build.py
</pre>

If all goes well, one can cd into ns-3-dev and run the ns-3 tests:

<pre>cd ns-3-dev
./waf check
</pre>

# <span id="Using_cygwin">Using cygwin</span>

Cygwin works reasonably-well by default: just make sure you grab the cygwin installer from <http://www.cygwin.com/setup.exe>. cygwin includes support for mercurial, gcc, and, python so, nothing else should be needed.

# <span id="Using_mingw">Using mingw</span>

Although we do not really support mingw because it requires more work to setup than cygwin, some people do use it with ns-3.

The following instructions are not complete and are not for the faint of heart. First, grab:

  * The core mingw system: <http://downloads.sourceforge.net/mingw/MinGW-5.1.4.exe>
  * python: <http://www.python.org/ftp/python/2.5.2/python-2.5.2.msi>

Since msvc cannot build ns-3, you need to tell WAF to use your mingw g++ compiler instead:

<pre>./waf configure --check-cxx-compiler=g++
</pre>

You might need additional packages, depending on your needs:

  * the [msys](http://downloads.sourceforge.net/mingw/MSYS-1.0.10.exe) shell. Warning: you should not use the msys terminal, as it does not play well with native windows binaries, such as Python. Just put the path to the msys binaries in the system PATH environment variable (My Computer -> Properties -> Advanced), but \_do NOT\_ run the msys terminal. Instead, run a plain old Windows terminal;
  * [mercurial](http://mercurial.selenic.com/downloads/). You will need this if you plan to use mercurial.
