---
title: Roadmap
layout: page
permalink: /develop/roadmap/
---
Because ns-3 is mostly developed through volunteer contributions by individuals all over the world, it is not really possible to commit to the implementation of specific features within explicit timeframes. The following is thus a summary of what a number of maintainers and developers of ns-3 modules plan to work on in the future. New features not present here might be implemented independently and merged within an ns-3 release and it could be that some of the features described below are never implemented due to shifting priorities.

# Build system

Move towards a more modular build system. A lot of work towards that goal has already been accomplished but a number of items are left:

  * Documentation should be located in each module. Need to figure out how to collect all documentations in a central location
  * Python bindings: each module should define its own bindings separately from the other modules.

The [bake tool](http://www.nsnam.org/wiki/BakeIntegration) is planned to help in this regard.

# Simulation core

  * Object model: add support for more complex attribute types. i.e., structures, and maybe arrays.
  * Object model: improve the type medatata to record information about which objects can be interconnected with which objects. The goal here is to allow the automated dump of a complete experiment topology in an xml file as well as allow reading these experiment descriptions to re-run an experiment
  * Scheduling API: introduce the concept of a logical process with lookahead in the API to make the MPI support more transparent
  * Time API: introduce the int64x64_t type and make the Time class use it.
  * Time API: allow global switch to control which unit is used to print Time objects.
  * [Starting and stopping of objects](http://www.nsnam.org/wiki/Object_Start_Stop_Specification) </ul> 
    # Direct Code Execution 
    
    Release of the [Direct Code Execution](http://www.nsnam.org/projects/direct-code-execution/) environment is scheduled for ns-3.17.
    
    # Data Collection and Experiment Control
    
    * [Data collection](http://www.nsnam.org/wiki/Data_Collection_Framework)
  
    * [Experiment control](http://www.nsnam.org/wiki/NSF_Frameworks)
