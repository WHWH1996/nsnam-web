---
title: Mailing lists
layout: page
permalink: /develop/tools/mailing-lists/
---

People interested in ns-3 development should subscribe to and read the ns-developers mailing list: [Join](http://www.isi.edu/nsnam/ns/ns-dev-list.html)&nbsp;&nbsp;[Archives](http://mailman.isi.edu/pipermail/ns-developers/) 

A few additional developer-oriented lists are still in some use but are gradually being phased out in favor of using GitLab notifications:
* ns-3-reviews: ns-3 code reviews. [Join/Archives](http://groups.google.com/group/ns-3-reviews) 
* ns-commits: every Mercurial commit pushed to one of the repositories hosted on code.nsnam.org generates an email describing the commit, and nightly validation messages. [Join](http://mailman.isi.edu/mailman/listinfo/ns-commits)&nbsp;&nbsp;[Archives](http://mailman.isi.edu/pipermail/ns-commits/) 
* ns-bugs: Automatic mail from Bugzilla database updates. [Join](http://mailman.isi.edu/mailman/listinfo/ns-bugs)&nbsp;&nbsp;[Archives](http://mailman.isi.edu/pipermail/ns-bugs/)

For user-oriented mail (getting help with using ns-3) there is also the <a name="ns-3-users"></a>**ns-3-users** mailing list: [Google Groups page](https://groups.google.com/forum/?fromgroups#!forum/ns-3-users)
