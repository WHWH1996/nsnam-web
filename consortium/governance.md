---
title: Governance
layout: page
permalink: /consortium/governance/
---
The formal structure and obligations of the University of Washington NS-3 Consortium is described in the Bylaws forming Exhibit A of the [NS-3 Consortium Affiliation and Membership Agreement](/docs/consortium/NS3_Consortium_Agreement_Bylaws.020619.pdf) that has been signed by the University of Washington and all members.  The following text provides a high-level overview of the way the NS-3 Consortium works.

# Officers

The NS-3 Consortium Officers consist of a Director and an Associate Director, both from the University of Washington, reporting to the Chair of the Department
of Electrical & Computer Engineering.

# Members

Members apply to join the consortium by submitting an application and
membership agreement, reviewed by the University of Washington.  Member
organizations are divided into the categories of Executive Member and
Consortium Member, and further, three classes of Consortium Members
exist; membership classes are based on the details of the organization.

# Advisory Board

The Advisory Board shall be comprised of the Director, Associate Director, two representatives designated by the Founding Executive Member, a representative designated by each Executive Member, and one member appointed by each Consortium Member. The function of the Advisory Board shall be to advise the UW and Director on all matters pertaining to the NS3C. The Advisory Board will meet at least annually and at NS3C’s annual meeting, at which time it will review and recommend goals and assess NS3C’s progress and financial condition. In addition, upon request, the Board will review research and program proposals, and may seek third party opinions and recommendations.

The current Advisory Board consists of:

  * Tom Henderson (Director, University of Washington)
  * Sumit Roy (Associate Director, University of Washington)
  * Walid Dabbous (INRIA)
  * Damien Saucez (INRIA)
  * Lorenza Giupponi (CTTC)
  * Manuel Ricardo (INESC TEC)
  * Doug Blough (Georgia Institute of Technology)
  * Mohit Tahiliani (NITK Surathkal)
  * Hui Liu (CMMB Vision)
  * Greg White (CableLabs)
  * Xiaojun Hei (HUST)

# Executive Members

Additional institutions may be be invited to become Executive Members by the University of Washington.  Executive Members are not subject to annual dues.


# Consortium Members

Not-for-profit organizations, small, and large companies may [apply](/consortium/memberships/join) to become Consortium Members. Each Consortium Member may:

  * Submit suggestions, requests and feedback concerning the ns-3 software development directions and roadmap, to be discussed during the yearly plenary assembly.
  * Attend the Consortium annual meeting during which the ns-3 most recent release shall be presented and during which a sample of suggestions made by Consortium Members shall be discussed.
  * Designate attendees (1 for small companies and universities and 2 for large companies) to attend a yearly, one day ns-3 training course.
  * Receive yearly Consortium financial and technical summary report.
  * Appoint a representative to the Advisory Board
  * Have their name, including logo, placed on the ns-3 website www.nsnam.org.

To inquire about participating, please email <consortium@nsnam.org>.
