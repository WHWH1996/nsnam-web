---
title: Meetings
layout: page
permalink: /consortium/activities/meetings/
---
This page provides meeting materials for the current ns-3 Consortium, starting
in June 2019.

 * June 2019:  <a href="https://www.nsnam.org/wp-content/uploads/2019/ns-3-annual-meeting-slides.pdf" target="_blank">annual meeting slides</a>
