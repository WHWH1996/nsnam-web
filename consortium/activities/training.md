---
title: Training
layout: page
permalink: /consortium/activities/training/
---
ns-3 training sessions are fee-based events intended to both educate new users and raise funding for the project. ns-3 Consortium members may designate attendees (1 for small companies and universities and 2 for large companies) to attend training for free as part of their membership. Student training is offered at reduced rates.

<h2>
  2019 
</h2>

ns-3 training was held on June 17-18, 2019 at the [ns-3 annual meeting](/research/wns3/wns3-2019/) in Florence, Italy. Materials (slides and video recordings) are available from the [wiki page](https://www.nsnam.org/wiki/AnnualTraining2019).

<h2>
  2018 
</h2>

Two days of training were provided at the <a href="https://www.nsnam.org/overview/wns3/wns3-2018/" target="_blank">ns-3 Consortium annual meeting</a> on June 11-12, 2018, at NITK Surathkal. <a href="http://www.nsnam.org/workshops/wns3-2018/ns-3-annual-meeting-2018.pdf" target="_blank"><b>Meeting flyer</b></a>&nbsp;and&nbsp;<a href="https://www.nsnam.org/docs/consortium/training/ns-3-training-2018.pdf" target="_blank"><b>training flyer</b>.

<h2>
  2017
</h2>

<p>
  One day of training was held at the ns-3 Consortium annual meeting on June 12, 2017, in Porto. <a href="https://www.nsnam.org/wiki/AnnualTraining2017" target="_blank">Training wiki page</a>
</p>

<h2>
  2016
</h2>

<p>
  Training was offered at the <a href="http://www.ee.washington.edu/events/wns3_2016/" target="_blank">ns-3 Consortium annual meeting</a> on June 13-14, in Seattle WA.
</p>

<p>
  Selected videos are available at <a target="_blank" href="https://vimeo.com/album/4614897">this site</a>.
</p>

<h2>
  2015
</h2>

<p>
  ns-3 training was recently offered at the <a href="/wp-content/uploads/2015/02/ns-3-training-2015.pdf">2015 annual meeting</a>. A team of ns-3 maintainers presented two days of training on May 11-12, 2015, covering an overview of the simulator, and specific focus on distributed simulations, vehicular communications, LTE, and Direct Code Execution. A training <a target="_blank" href="https://www.nsnam.org/wiki/AnnualTraining2015">wiki page</a> provides additional details about these sessions.
</p>

<p>
  The ns-3 Consortium and CTTC have made available <a target="_blank" href="http://vimeo.com/album/3480129">training videos</a> that were recorded for all sessions. Slides are available from the wiki page linked above.
</p>

<h2>
  2014
</h2>

<p>
  A team of ns-3 maintainers presented two days of training on May 5-6, 2014, in Atlanta GA, covering an overview of the simulator, and specific focus on ns-3 tracing, WiFi and LTE models, distributed simulations, emulation, and Direct Code Execution. A training <a target="_blank" href="http://www.nsnam.org/wp-content/uploads/2014/02/ns-3-training-2014.pdf">flyer</a> provides additional details about these sessions.
</p>

<p>
  Attendees of the sessions and ns-3 Consortium members have access to the <a target="_blank" href="http://vimeo.com/album/2966916">training videos</a> that were recorded for all sessions. Slides used during the training sessions are posted below:
</p>

<ul>
  <li>
    Session 1: <a target="_blank" href="https://www.nsnam.org/tutorials/consortium14/ns-3-training-session-1.pdf">ns-3 introduction</a>
  </li>
  <li>
    Session 2: <a target="_blank" href="https://www.nsnam.org/tutorials/consortium14/ns-3-training-session-2.pdf">ns-3 core</a>
  </li>
  <li>
    Session 3: <a target="_blank" href="https://www.nsnam.org/tutorials/consortium14/ns-3-training-session-3.pdf">ns-3 tracing</a>
  </li>
  <li>
    Session 4: <a target="_blank" href="https://www.nsnam.org/tutorials/consortium14/ns-3-training-session-4.pdf">writing ns-3 code</a>
  </li>
  <li>
    Session 5: <a target="_blank" href="https://www.nsnam.org/tutorials/consortium14/ns-3-training-session-5.pdf">ns-3 WiFi</a>
  </li>
  <li>
    Session 6: <a target="_blank" href="https://www.nsnam.org/tutorials/consortium14/ns-3-training-session-6.pdf">ns-3 LTE</a>
  </li>
  <li>
    Session 7 <a target="_blank" href="https://www.nsnam.org/tutorials/consortium14/ns-3-training-session-7.pdf">ns-3 distributed</a>
  </li>
  <li>
    Session 8: <a target="_blank" href="https://www.nsnam.org/tutorials/consortium14/ns-3-training-session-8.pdf">ns-3 emulation and DCE</a>
  </li>
</ul>
