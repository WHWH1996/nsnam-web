---
title: Join
layout: page
permalink: /consortium/memberships/join/
---
Organizations may become Consortium Members by submitting an [application agreement](/docs/consortium/NS3_Consortium_Agreement_Bylaws.020619.pdf) to the University of Washington (Director or Associate Director) or to <consortium@nsnam.org> and agreeing to pay the annual fees associated with membership.&nbsp; The fee structure is as follows:

  * Universities, non-profits, FFRDCs: $1,500
  * Very small companies (fewer than 20 employees): $1,500
  * Small companies (20-500 employees): $7,500
  * Large companies (greater than 500 employees): $15,000
		  
