---
title: ns-3.20
layout: page
permalink: /releases/ns-3-20/
---
<div>
  <p>
    ns-3.20 was released on 17 June 2014. This release features a new LR-WPAN model providing initial support for IEEE 802.15.4 networks, and integration with the previously released SixLowPan (6LoWPAN) module. A new IPv6 routing protocol (RIPng) model has been added, and support within the Flow Monitor for IPv6 has been added. A new LTE MAC downlink scheduling algorithm named Channel and QoS Aware (CQA) Scheduler is provided by the new CqaFfMacScheduler object. In addition, the <a href="http://code.nsnam.org/ns-3.20/file/5f2f0408cdc0/RELEASE_NOTES">RELEASE_NOTES</a> list the many bugs fixed and small improvements made.
  </p>

  <ul>
    <li>
      The latest ns-3.20 source code can be downloaded from <a href="https://www.nsnam.org/release/ns-allinone-3.20.tar.bz2">here</a>
    </li>
    <li>
      The documentation is available in several formats from <a href="/releases/ns-3-20/documentation">here</a>.
    </li>
    <li>
      Errata containing late-breaking information about the release can be found <a href="http://www.nsnam.org/wiki/Errata">here</a>
    </li>
    <li>
      A patch to upgrade from the last release (ns-3.19) can be found <a href="https://www.nsnam.org/release/patches/ns-3.19-to-ns-3.20.patch">here</a>
    </li>
  </ul>
</div>
