---
title: Download
layout: page
permalink: /releases/ns-3-28/download/
---
Please click the following link to download ns-3.28.1, released August 2018:

  * [ns-allinone-3.28.1](/releases/ns-allinone-3.28.1.tar.bz2) (compressed source code archive)

If you are already using ns-3.28 and wish to patch upgrade to ns-3.28.1, a patch is available [here](/releases/patches/ns-3.28-to-ns-3.28.1.patch).

If you wish to download the initial ns-3.28 release from March 2018, that source code is available here:

  * [ns-allinone-3.28](/releases/ns-allinone-3.28.tar.bz2) (compressed source code archive)

A source code patch to update ns-3.27 release to ns-3.28 release is available [here](/releases/patches/ns-3.27-to-ns-3.28.patch). Other patches to migrate older versions of ns-3 (back to ns-3.17) to the latest version can be found in the same directory; they must be applied sequentially to upgrade across multiple releases.
