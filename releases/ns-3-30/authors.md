---
title: Authors
layout: page
permalink: /releases/ns-3-30/authors/
---
The following people made source code contributions (commits) during the ns-3.30 development period:

  * Gabriel Arrobo (gab.arrobo@gmail.com)
  * Zoraze Ali (zoraze.ali@cttc.es)
  * Stefano Avallone (stavallo@unina.it) 
  * Vignesh Babu (ns3-dev@esk.fraunhofer.de)
  * Shikha Bakshi (shikhabakshi912@gmail.com)
  * Peter D. Barnes, Jr. (barnes26@llnl.gov) 
  * Pedro Bellotti (pedro177@gmail.com)
  * Apoorva Bhargava (apoorvabhargava13@gmail.com)
  * Micolaj Chwalisz (chwalisz@tkn.tu-berlin.de)
  * Sébastien Deronne (sebastien.deronne@gmail.com) 
  * Tom Henderson (tomh@tomh.org) 
  * Jack Higgins (shattered.feelings@gmail.com)
  * Muhammad Inamullah (inamullah.m@gmail.com)
  * Vignesh Kannan (vignesh2496@gmail.com)
  * Alexander Krotov (krotov@iitp.ru) 
  * Harsh Lara (harshapplefan@gmail.com)
  * Davide Magrin (magrin.davide@gmail.com)
  * Ryan Mast (mast9@llnl.gov)
  * Alfonso Oliveira (af.oliveira.16@gmail.com)
  * Jendaipou Palmei (jendaipoupalmei@gmail.com)
  * Natale Patriciello (natale.patriciello@gmail.com) 
  * Tommaso Pecorella (tommaso.pecorella@unifi.it) 
  * Eduardo Pinto (epmcj@hotmail.com)
  * Michele Polese (michele.polese@gmail.com) 
  * Alberto Gallegos Ramonet (ramonet@fc.ritsumei.ac.jp)
  * Getachew Redieteab (getachew.redieteab@orange.com)
  * Manuel Requena (manuel.requena@cttc.es) 
  * Richard Sailer (richard_sailer@systemli.org)
  * Steve Smith (smith84@llnl.gov) 
  * Tinghui Wang (tinghui.wang@wsu.edu)
