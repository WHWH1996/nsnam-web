---
id: 3104
title: WNS3 Call for Posters, Demos, Short Talks
date: 2015-02-24T05:42:16+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3104
permalink: /events/wns3-call-for-posters-demos-short-talks/
categories:
  - Events
---
The [Workshop on ns-3 (WNS3)](http://www.nsnam.org/wns3/wns3-2015/) invites your participation in the workshop with a short presentation about work-in-progress, a poster presentation, and/or a demo presentation. We are planning to organize events in addition to the regular paper track, to create more opportunities for discussion in the workshop. The call for posters and demonstrations can be found [here](/wns3/wns3-2015/call-for-demos/).

WNS3 will be part of a [week-long series of events](/wp-content/uploads/2015/02/ns-3-annual-2015.pdf) in Barcelona, including ns-3 training, the consortium annual meeting, and developer meetings.