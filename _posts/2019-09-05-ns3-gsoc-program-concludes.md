---
title: ns-3 GSoC program concludes
date: 2019-09-05T18:00:00+00:00
author: tomh
layout: post
permalink: /news/2019-ns-3-gsoc-program-concludes/
categories:
  - News
---
Four students successfully completed Google Summer of Code with the ns-3 project:
 * Tommaso Zugno, Integration of the 3GPP TR 38.901 channel model in the ns-3 spectrum module
 * Liangcheng Yu, Framework of Studying Flow Completion Time Minimization for Data Center Networks in ns-3
 * Mishal Shah, Improving the ns-3 AppStore and linking with bake
 * Apoorva Bhargava, TCP Testing and Alignment 
Thanks to both the students and mentors for their efforts.  A project summary of each project can be found <a href="https://www.nsnam.org/wp-content/uploads/2019/ns-3-gsoc-2019-wrapup.pdf">here</a>.
