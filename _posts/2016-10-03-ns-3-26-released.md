---
id: 3606
title: ns-3.26 released
date: 2016-10-03T21:20:27+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3606
permalink: /news/ns-3-26-released/
categories:
  - News
  - ns-3 Releases
---
ns-3.26 was released on 3 October 2016 and features the following significant changes. 

  * A new class **SpectrumWifiPhy** has been introduced that makes use of the Spectrum module. Its functionality and API is currently very similar to that of the YansWifiPhy model, especially because it reuses the same InterferenceHelper and ErrorModel classes for this release (although newer error models are now possible).
  * Several new TCP congestion control variants were introduced, including **TCP Vegas, Scalable, Veno, Illinois, Bic, YeAH, and H-TCP** congestion control algorithms. 
      * The traffic control module adds models for **FQ-CoDel**, **PIE**, and **Byte Queue Limits**. Extensions were also made to integrate the Wi-Fi module with the traffic control sublayer. A **WifiNetDevice::SelectQueue** method has been added to determine the user priority of an MSDU, to allow the traffic control layer to align with QoS-aware queues in the Wifi device. As part of the traffic control work, the API for passing QoS and priority from applications down through the Internet stack has been updated, and the PfifoFast queue disc now classifies packets into bands based on their priority. 
      * The Wi-Fi module includes better support for **IEEE 802.11e** features including TXOP limits, and the API for configuring Wi-Fi channel number, center frequency, and standard has been made more consistent. </ul> 
    Finally, the release includes numerous bug fixes and small improvements, listed in the <a href="http://code.nsnam.org/ns-3.26/raw-file/46d53abb44ad/RELEASE_NOTES" target="_blank">RELEASE_NOTES</a>.