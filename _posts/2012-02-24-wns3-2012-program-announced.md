---
id: 1618
title: WNS3 2012 Program Announced
date: 2012-02-24T11:10:11+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=1618
permalink: /events/wns3-2012-program-announced/
categories:
  - Events
tags:
  - simutools
  - wns3
---
The [preliminary program for WNS3 2012](http://www.nsnam.org/wns3/wns3-2012/wns3-2012-program/) is now available. The event is scheduled to happen on the 23rd of March, at Sirmione, Italy.

We have an exciting lineup of presentations for the day, along with a shared poster/demo session with the Omnet++ community. We are happy to announce that Tom Henderson and Mathieu Lacage will be delivering keynote talks as well.

We welcome all members from the ns-3 community to attend and participate in the event, and we look forward to meeting you all.

&nbsp;

&#8212;

Pavel Boyko and Lalith Suresh
  
WNS3 2012 TPC Chairs