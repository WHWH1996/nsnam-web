---
id: 3116
title: ns-3 accepted to Google Summer of Code 2015
date: 2015-03-02T19:30:29+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3116
permalink: /news/ns-3-accepted-to-google-summer-of-code-2015/
categories:
  - Events
  - News
---
ns-3 is participating in GSoC 2015! We were happy to learn today that we will be one of 137 organizations participating in the 2015 [Google Summer of Code](http://www.google-melange.com/gsoc/homepage/google/gsoc2015). This program is a great opportunity for students to learn a bit about software engineering and open source projects, and for our project community to grow. Interested students are encouraged to interact with the project through the main project mailing list, [ns-developers@isi.edu](http://www.nsnam.org/developers/tools/mailing-lists/), and to review our [wiki](http://www.nsnam.org/wiki/index.php/GSOC2015StudentGuide). Students will have until March 27 to learn about ns-3, develop a project proposal, and submit it to Google.