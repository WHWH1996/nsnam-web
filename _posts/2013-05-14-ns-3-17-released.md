---
id: 2370
title: ns-3.17 released
date: 2013-05-14T21:39:19+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2370
permalink: /news/ns-3-17-released/
categories:
  - Events
  - News
  - ns-3 Releases
---
The ns-3.17 release has now been posted at <http://www.nsnam.org/release/ns-allinone-3.17.tar.bz2>

This release adds several new models for LTE, TCP, and network emulation, and also features the initial release of the [Direct Code Execution](http://www.nsnam.org/overview/projects/direct-code-execution/) environment, enabled by the [Bake software integration tool](http://www.nsnam.org/docs/bake/tutorial/html/index.html). More details are available [here.](http://www.nsnam.org/ns-3-17/)