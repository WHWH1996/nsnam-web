---
id: 2952
title: DCE demonstration at ACM MSWiM
date: 2014-09-22T23:40:42+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2952
permalink: /events/dce-demonstration-at-acm-mswim/
categories:
  - Events
---
A team from INRIA, France and the University of Tokyo presented a <a target="_blank" href="http://www.nsnam.org/overview/projects/direct-code-execution/">Direct Code Execution (DCE)</a> demonstration at the <a href="http://mswimconf.com/2014/" target="_blank">17th ACM International Conference on Modeling, Analysis and Simulation of Wireless and Mobile Systems (MSWiM)</a> in Montreal on September 22. Entitled &#8220;Realistic Evaluation of Kernel protocols and Software Defined Wireless Networks with DCE/ns-3,&#8221; the demonstration showcased two main scenarios: (1) a basic example describing how to integrate in DCE the Data Center TCP (DCTCP) Linux kernel patch, and then how to customize this protocol and run it on different scenarios; and (2) a more advanced use case demonstrating how to benefit from DCE to build a rich and realistic evaluation environment for Software Defined Wireless Networks based on Open vSwitch and the NOX SDN controller. DCE is a framework enabling the execution of real application and Linux kernel code within an ns-3 simulation context, allowing for a high degree of implementation realism and debugging capabilities. Full support for SDN is planned for the next DCE release.