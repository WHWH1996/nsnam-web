---
id: 4013
title: ns-3 selected for Google Code-In
date: 2018-09-18T21:50:56+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=4013
permalink: /news/ns-3-selected-for-google-code-in/
categories:
  - Events
  - News
---
We were pleased to learn today that ns-3 has been selected as a mentoring organization for Google Code-In (GCI) 2018. This is our project&#8217;s first time with this programming contest. Mohit Tahiliani will lead a team of mentors designing and evaluating programming tasks relating to coding, documentation, training, outreach, research, quality assurance, and design. More information can be found on [our wiki](/wiki/GCI2018Details).
